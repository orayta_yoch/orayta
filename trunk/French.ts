<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name></name>
    <message>
        <source>Open in new tab</source>
        <translation type="obsolete">Ouvrir dans un nouvel onglet</translation>
    </message>
    <message>
        <source>Edit bookmark title...</source>
        <translation type="obsolete">Editer le marque-page...</translation>
    </message>
    <message>
        <source>Delete bookmark</source>
        <translation type="obsolete">Supprimer le marque page</translation>
    </message>
</context>
<context>
    <name>About</name>
    <message>
        <location filename="about.ui" line="14"/>
        <source>About Orayta</source>
        <translation>A propos de Orayta</translation>
    </message>
    <message>
        <location filename="about.cpp" line="26"/>
        <source>Orayta - Hebrew Books</source>
        <oldsource>&lt;center&gt;&lt;b&gt;Orayta - Hebrew Books</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="about.cpp" line="41"/>
        <location filename="about.cpp" line="42"/>
        <source>5770</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="about.cpp" line="43"/>
        <source>Torat Emet software</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddBookMark</name>
    <message>
        <location filename="addbookmark.ui" line="17"/>
        <source>Bookmark&apos;s title</source>
        <translation type="unfinished">Titre du marque-page</translation>
    </message>
    <message>
        <location filename="addbookmark.ui" line="26"/>
        <source>Bookmark&apos;s title:</source>
        <translation type="unfinished">Titre du marque-page:</translation>
    </message>
    <message>
        <location filename="addbookmark.ui" line="42"/>
        <source>Cancel</source>
        <translation type="unfinished">Annuler</translation>
    </message>
    <message>
        <location filename="addbookmark.ui" line="62"/>
        <source>OK</source>
        <translation type="unfinished">OK</translation>
    </message>
</context>
<context>
    <name>AddComment</name>
    <message>
        <location filename="addcomment.ui" line="17"/>
        <source>Add comment</source>
        <translation>Ajouter un commentaire</translation>
    </message>
    <message>
        <location filename="addcomment.ui" line="41"/>
        <source>Delete comment</source>
        <translation>Supprimer un commentaire</translation>
    </message>
    <message>
        <location filename="addcomment.ui" line="64"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="addcomment.ui" line="71"/>
        <source>Save</source>
        <translation>Sauvegarder</translation>
    </message>
</context>
<context>
    <name>BaseHtmlView</name>
    <message>
        <location filename="bookview_html.cpp" line="25"/>
        <source>Copy text</source>
        <translation type="unfinished">Copier le texte</translation>
    </message>
    <message>
        <location filename="bookview_html.cpp" line="178"/>
        <source>404 - Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="bookview_html.cpp" line="178"/>
        <source>file not found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BookMarkTitle</name>
    <message>
        <source>Bookmark&apos;s title</source>
        <translation type="obsolete">Titre du marque-page</translation>
    </message>
    <message>
        <source>Bookmark&apos;s title:</source>
        <translation type="obsolete">Titre du marque-page:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
</context>
<context>
    <name>BookTree</name>
    <message>
        <location filename="booktree.cpp" line="83"/>
        <source>Orayta Library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="booktree.cpp" line="84"/>
        <source>User Library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="booktree.cpp" line="91"/>
        <source>Open book</source>
        <translation type="unfinished">Ouvrir le livre</translation>
    </message>
    <message>
        <location filename="booktree.cpp" line="92"/>
        <source>Open in new tab</source>
        <translation type="unfinished">Ouvrir dans un nouvel onglet</translation>
    </message>
    <message>
        <location filename="booktree.cpp" line="93"/>
        <source>Delete book</source>
        <translation type="unfinished">Supprimer ce livre</translation>
    </message>
    <message>
        <location filename="booktree.cpp" line="94"/>
        <source>Change font</source>
        <translation type="unfinished">Changer la police</translation>
    </message>
    <message>
        <location filename="booktree.cpp" line="374"/>
        <source>Are you sure you want to remove this </source>
        <translation type="unfinished">Etes vous sûr de vouloir supprimer ce </translation>
    </message>
    <message>
        <location filename="booktree.cpp" line="375"/>
        <source>books directory ?
This will remove all the books in this directory.</source>
        <translation type="unfinished">repertoire ?
Ceci supprimera également tous les livres présents dans ce repertoire.</translation>
    </message>
    <message>
        <location filename="booktree.cpp" line="376"/>
        <source>book ?</source>
        <translation type="unfinished">livre ?</translation>
    </message>
    <message>
        <location filename="booktree.cpp" line="378"/>
        <source>Deleting book</source>
        <translation type="unfinished">Suppression</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="100"/>
        <location filename="mainwindow.cpp" line="391"/>
        <source>Orayta</source>
        <translation></translation>
    </message>
    <message>
        <source>No books found! 
Check your installation, or contact the developer.</source>
        <translation type="obsolete">Aucun livre trouvé !
Verifiez votre installation, ou contactez le devloppeur.</translation>
    </message>
    <message>
        <source>Loading...</source>
        <translation type="obsolete">Chargement...</translation>
    </message>
    <message>
        <source>Are you sure you want to remove this </source>
        <translation type="obsolete">Etes vous sûr de vouloir supprimer ce </translation>
    </message>
    <message>
        <source>books directory ?
This will remove all the books in this directory.</source>
        <translation type="obsolete">repertoire ?
Ceci supprimera également tous les livres présents dans ce repertoire.</translation>
    </message>
    <message>
        <source>book ?</source>
        <translation type="obsolete">livre ?</translation>
    </message>
    <message>
        <source>Deleting book</source>
        <translation type="obsolete">Suppression</translation>
    </message>
    <message>
        <source>Delete book</source>
        <translation type="obsolete">Supprimer ce livre</translation>
    </message>
    <message>
        <source>Change font</source>
        <translation type="obsolete">Changer la police</translation>
    </message>
    <message>
        <source>Page: </source>
        <translation type="obsolete">Page: </translation>
    </message>
    <message>
        <source>Add bookmark here...</source>
        <translation type="obsolete">Ajouter un marque-page ici...</translation>
    </message>
    <message>
        <source>Add/edit comment...</source>
        <translation type="obsolete">Ajouter/Editer un commentaire...</translation>
    </message>
    <message>
        <source>Edit comment...</source>
        <translation type="obsolete">Editer le commentaire...</translation>
    </message>
    <message>
        <source>Delete comment</source>
        <translation type="obsolete">Supprimer le commentaire</translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="161"/>
        <source>Searching: </source>
        <translation>Recherche: </translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="165"/>
        <source>Search results: </source>
        <translation>Résultats :</translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="210"/>
        <location filename="mainwnd_search.cpp" line="341"/>
        <source>(Search stopped by user)</source>
        <translation>(Recherche stoppée par l&apos;utilisateur)</translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="217"/>
        <source>No search results found:</source>
        <translation>Aucun résultat:</translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="225"/>
        <source>firsts results only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="229"/>
        <source> entries founds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="297"/>
        <source>Searching guematria for: </source>
        <translation>Recherche la guematria de: </translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="330"/>
        <source>Search results for guematria : </source>
        <translation>Résultats pour la quematria de : </translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="348"/>
        <source>No tanach books selected : please select books in tanach and search again.</source>
        <oldsource>Any tanach books selected : please select books in tanach and search again.</oldsource>
        <translation>Aucun livre du tanach dans la selection : sélectionnez svp un ou plusieurs livres avant de relancer la recherche.</translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="354"/>
        <source>No guematria results found:</source>
        <translation>Aucune guematria trouvée:</translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="361"/>
        <source>Result list: </source>
        <translation>Liste des résultats: </translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="362"/>
        <source> results founds.</source>
        <oldsource> results founds.&lt;br&gt;&lt;br&gt;</oldsource>
        <translation>résultats trouvés.</translation>
    </message>
    <message>
        <source>No serach results found:</source>
        <translation type="obsolete">Aucun résultat :</translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="222"/>
        <source>Short result list: </source>
        <translation>Liste des résultats: </translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="233"/>
        <source>Full result list:</source>
        <translation>Liste complète:</translation>
    </message>
    <message>
        <source>Open book</source>
        <translation type="obsolete">Ouvrir le livre</translation>
    </message>
    <message>
        <source>Open in new tab</source>
        <translation type="obsolete">Ouvrir dans un nouvel onglet</translation>
    </message>
    <message>
        <location filename="mainwnd_bookmark.cpp" line="26"/>
        <source>Edit bookmark title...</source>
        <translation>Editer le marque-page...</translation>
    </message>
    <message>
        <location filename="mainwnd_bookmark.cpp" line="27"/>
        <source>Delete bookmark</source>
        <translation>Supprimer le marque page</translation>
    </message>
</context>
<context>
    <name>MainWindowClass</name>
    <message>
        <location filename="mainwindow.ui" line="26"/>
        <source>Orayta - Hebrew books</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="502"/>
        <source>Orayta</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="508"/>
        <source>Open new tab</source>
        <translation>Ouvrir un nouvel onglet</translation>
    </message>
    <message>
        <source>Search:</source>
        <translation type="obsolete">Recherche:</translation>
    </message>
    <message>
        <source>Search in selected books</source>
        <translation type="obsolete">Chercher dans les livres sélectionnés</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="725"/>
        <source>Search backwards in book</source>
        <translation>chercher en arriére dans le livre</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="754"/>
        <source>Search forward in book</source>
        <translation>Chercher en avant dans le livre</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="560"/>
        <source>Zoom out</source>
        <translation>Réduire</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="589"/>
        <source>Zoom in</source>
        <translation>Agrandir</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="531"/>
        <source>Jump to top</source>
        <translation>Revenir au début</translation>
    </message>
    <message>
        <source>Pdf page:</source>
        <translation type="obsolete">Pdf page:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="618"/>
        <source>Search in current books</source>
        <translation>Cherche dans ce livre</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="845"/>
        <source>Books</source>
        <translation>Livres</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="873"/>
        <source>Add all books to search</source>
        <translation>Ajouter tous les livres à la recherche</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="681"/>
        <location filename="mainwindow.ui" line="876"/>
        <location filename="mainwindow.ui" line="890"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="887"/>
        <source>Remove all books from search</source>
        <translation>Supprimer tous les livres de la recherche</translation>
    </message>
    <message>
        <source>showAlone</source>
        <translation type="obsolete">Afficher seul</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1191"/>
        <source>Allow &quot;Ktiv Haser&quot;</source>
        <translation>Autorise le &quot;Ktiv &apos;Hasser&quot;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1392"/>
        <source>Bookmarks</source>
        <translation>Marque-pages</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1403"/>
        <source>Remove bookmark</source>
        <translation>Supprimer le marque-page</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1406"/>
        <source>Erase bookmark</source>
        <translation>Effacer le marque-page</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1432"/>
        <source>Move bookmark up</source>
        <translation>Monter le marque-page</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1461"/>
        <source>Move bookmark down</source>
        <translation>Descendre le marque-page</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1490"/>
        <source>Edit bookmark&apos;s title</source>
        <translation>Editer le titre du marque-page</translation>
    </message>
    <message>
        <source>Configure</source>
        <translation type="obsolete">Configuration</translation>
    </message>
    <message>
        <source>Default font:</source>
        <translation type="obsolete">Police par défaut:</translation>
    </message>
    <message>
        <source>Base font size:</source>
        <translation type="obsolete">Taille de la police par défaut:</translation>
    </message>
    <message>
        <source>font preview:</source>
        <translation type="obsolete">Prévisualisation:</translation>
    </message>
    <message>
        <source>(Make sure the font can handle Nikud and Teamim)</source>
        <translation type="obsolete">(Assurez vous que cette police affiche correctement le Nikud et les Teamim)</translation>
    </message>
    <message>
        <source>Save changes</source>
        <translation type="obsolete">sauvegarder les modifications</translation>
    </message>
    <message>
        <source>(Confs will apply as of next book)</source>
        <translation type="obsolete">(cette configuration prendra effet sur le prochain livre)</translation>
    </message>
    <message>
        <source>Gui language:</source>
        <translation type="obsolete">Langage:</translation>
    </message>
    <message>
        <source>Changing the language will restart the application, and close all open books</source>
        <translation type="obsolete">Changer le langage va redémmarer l&apos;aplication, et fermer tous les livres ouverts</translation>
    </message>
    <message>
        <source>Change language</source>
        <translation type="obsolete">Changer la langue</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="996"/>
        <source>Hide commentaries</source>
        <translation>Cacher les commentaires</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1018"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1076"/>
        <source>( The search ignores Punctuation,
        Nikud and Teamim. )
</source>
        <translation>(La recherche ignore la ponctuation, le Nikud et les Teamim)
</translation>
    </message>
    <message>
        <source>Find exact string</source>
        <translation type="obsolete">Cette expression exacte</translation>
    </message>
    <message>
        <source>Regular expression search</source>
        <translation type="obsolete">Rechercher une regex</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1209"/>
        <source>Guematria search (tanach only)</source>
        <translation>Rechercher une guematria (tanach uniquement)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1665"/>
        <source>&amp;File</source>
        <oldsource>File</oldsource>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1680"/>
        <source>&amp;Help</source>
        <oldsource>Help</oldsource>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1689"/>
        <source>&amp;Display</source>
        <oldsource>Display</oldsource>
        <translation>&amp;Affichage</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1050"/>
        <location filename="mainwindow.ui" line="1261"/>
        <source>Search</source>
        <translation>Recherche</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="711"/>
        <source>Search in book:</source>
        <translation>Recherche dans la page:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1061"/>
        <source>Search for:</source>
        <translation>Rechercher:</translation>
    </message>
    <message>
        <source>( The search ignores Punctuation, Nikud and Teamim. )
</source>
        <translation type="obsolete">( La recherche ignore la ponctuation, le Nikud et les Teamim)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1093"/>
        <source>String search</source>
        <translation>Texte recherché</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1162"/>
        <source>Find any of the words</source>
        <translation>Une partie des mots</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1131"/>
        <source>Find all words</source>
        <translation>Tous les mots</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1150"/>
        <source>spacing (in words)</source>
        <translation>espacement (en mots)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1179"/>
        <source>Find full words only</source>
        <translation>Mots entiers seulement</translation>
    </message>
    <message>
        <source>Allow &quot;Ktiv Maleh&quot; / &quot;Ktiv Haser&quot;</source>
        <translation type="obsolete">Autoriser &quot;Ktiv Maleh&quot; / &quot;Ktiv &apos;Hasser&quot;</translation>
    </message>
    <message>
        <source>Find regular expression</source>
        <translation type="obsolete">Utiliser les expressions regulières</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1302"/>
        <source>Searching ...</source>
        <translation>Recherche en cours...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1349"/>
        <source>Cancel search</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1584"/>
        <source>Add a bookmark here:</source>
        <translation>Ajouter un marque-page ici:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1601"/>
        <source>Title:</source>
        <translation>Titre:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1702"/>
        <source>&amp;Search</source>
        <translation>&amp;Recherche</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1724"/>
        <source>&amp;Location</source>
        <oldsource>Location</oldsource>
        <translation>&amp;Position</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1742"/>
        <source>E&amp;xit</source>
        <oldsource>Exit</oldsource>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1751"/>
        <source>&amp;About</source>
        <oldsource>About</oldsource>
        <translation>&amp;A propos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1760"/>
        <source>Zoom &amp;in</source>
        <translation>&amp;Agrandir</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1769"/>
        <source>Zoom &amp;out</source>
        <translation>&amp;Réduire</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1778"/>
        <source>&amp;Jump to top</source>
        <translation>Aller au &amp;début</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1787"/>
        <source>&amp;Print book</source>
        <oldsource>Print book</oldsource>
        <translation>Im&amp;prime le livre</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1796"/>
        <source>&amp;Close tab</source>
        <oldsource>Close tab</oldsource>
        <translation>&amp;Fermer l&apos;onglet</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1805"/>
        <source>&amp;Open new tab</source>
        <translation>&amp;Ouvrir dans un nouvel onglet</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1816"/>
        <source>Show nikud</source>
        <translation>Afficher le Nikud</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1827"/>
        <source>Show teamim</source>
        <translation>Afficher les Teamim</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1630"/>
        <source>Add bookmark</source>
        <translation>Ajouter un marque-page</translation>
    </message>
    <message>
        <source>refresh this book</source>
        <translation type="obsolete">recharger</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1110"/>
        <source>Find exact string (or regexp)</source>
        <translation>Phrase exacte (ou regex)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1836"/>
        <source>Add &amp;bookmark</source>
        <translation>Ajouter un &amp;signet</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1845"/>
        <source>Add/edit &amp;comment</source>
        <oldsource>Add/edit comment</oldsource>
        <translation>Ajouter/éditer un &amp;commentaire</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1854"/>
        <source>&amp;Report typo</source>
        <oldsource>Report typo</oldsource>
        <translation>&amp;Rapporter une erreur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1863"/>
        <source>&amp;Search in books</source>
        <oldsource>Search in books</oldsource>
        <translation>&amp;Rechercher dans la bibliothèque</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1872"/>
        <source>Search in current book</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1944"/>
        <source>&amp;Settings</source>
        <oldsource>Settings</oldsource>
        <translation>&amp;Préférences</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1953"/>
        <source>&amp;Import</source>
        <oldsource>Import</oldsource>
        <translation>&amp;Import</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1962"/>
        <source>Search &amp;guematria</source>
        <oldsource>Search guematria</oldsource>
        <translation>Rechercher une &amp;guematria</translation>
    </message>
    <message>
        <source>Advanced search</source>
        <translation type="obsolete">Recherche avancée</translation>
    </message>
    <message>
        <source>Search in book</source>
        <translation type="obsolete">Chercher dans le livre</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1881"/>
        <source>Search forward</source>
        <translation>Chercher en avant</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1890"/>
        <source>Search backwards</source>
        <translation>Chercher en arrière</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1899"/>
        <source>Remove book from search</source>
        <translation>Enlever ce livre de la recherche</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1908"/>
        <source>Add book to search</source>
        <translation>Ajouter ce livre à la recherche</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1917"/>
        <source>Add all to search</source>
        <translation>Tout ajouter à la recherche</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1926"/>
        <source>Remove all from search</source>
        <translation>Tout enlever de la recherche</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1935"/>
        <source>&amp;Find book by name</source>
        <oldsource>Find book by name</oldsource>
        <translation>&amp;Trouver un livre d&apos;aprés le titre</translation>
    </message>
</context>
<context>
    <name>OraytaBookView</name>
    <message>
        <location filename="bookview_orayta.cpp" line="19"/>
        <source>Add bookmark here...</source>
        <translation type="unfinished">Ajouter un marque-page ici...</translation>
    </message>
    <message>
        <location filename="bookview_orayta.cpp" line="20"/>
        <source>Add/edit comment...</source>
        <translation type="unfinished">Ajouter/Editer un commentaire...</translation>
    </message>
    <message>
        <location filename="bookview_orayta.cpp" line="21"/>
        <source>Delete comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="bookview_orayta.cpp" line="22"/>
        <source>Copy text only</source>
        <translation type="unfinished">Coiper le texte uniquement</translation>
    </message>
</context>
<context>
    <name>PdfBookView</name>
    <message>
        <location filename="bookview_pdf.cpp" line="75"/>
        <source>Page </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PdfWidget</name>
    <message>
        <location filename="pdfwidget.cpp" line="80"/>
        <source>Copy text</source>
        <translation>Copier le texte</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="functions.cpp" line="496"/>
        <source>LTR</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QtIOCompressor</name>
    <message>
        <location filename="qtiocompressor.cpp" line="129"/>
        <location filename="qtiocompressor.cpp" line="595"/>
        <source>Internal zlib error when compressing: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtiocompressor.cpp" line="161"/>
        <source>Error writing to underlying device: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtiocompressor.cpp" line="362"/>
        <source>Error opening underlying device: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtiocompressor.cpp" line="532"/>
        <source>Error reading data from underlying device: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtiocompressor.cpp" line="552"/>
        <source>Internal zlib error when decompressing: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QtIOCompressor::open</name>
    <message>
        <location filename="qtiocompressor.cpp" line="396"/>
        <source>The gzip format not supported in this version of zlib.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtiocompressor.cpp" line="412"/>
        <source>Internal zlib error: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchForm</name>
    <message>
        <source>Advanced search</source>
        <translation type="obsolete">Recherche avancée</translation>
    </message>
    <message>
        <source>String search</source>
        <translation type="obsolete">Texte recherché</translation>
    </message>
    <message>
        <source>Find all words</source>
        <translation type="obsolete">Tous les mots</translation>
    </message>
    <message>
        <source>Find any of the words</source>
        <translation type="obsolete">Une partie des mots</translation>
    </message>
    <message>
        <source>Find full words only</source>
        <translation type="obsolete">Mots entiers seulement</translation>
    </message>
    <message>
        <source>Find regular expression</source>
        <translation type="obsolete">Utiliser les expressions regulières</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="obsolete">Rechercher</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="settings.ui" line="14"/>
        <source>Orayta settings</source>
        <translation>Réglages Orayta</translation>
    </message>
    <message>
        <location filename="settings.ui" line="24"/>
        <source>Font</source>
        <translation>Police</translation>
    </message>
    <message>
        <location filename="settings.ui" line="36"/>
        <source>Default font:</source>
        <translation>Police par défaut:</translation>
    </message>
    <message>
        <location filename="settings.ui" line="46"/>
        <source>font: 8pt &quot;Sans Serif&quot;;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings.ui" line="76"/>
        <source>Base font size:</source>
        <translation>Taille de la police par défaut:</translation>
    </message>
    <message>
        <location filename="settings.ui" line="99"/>
        <source>Font preview:</source>
        <translation>Prévisualisation:</translation>
    </message>
    <message>
        <location filename="settings.ui" line="114"/>
        <source>(Make sure the font can handle Nikud and Teamim)</source>
        <translation>(Assurez vous que cette police affiche correctement le Nikud et les Teamim)</translation>
    </message>
    <message utf8="true">
        <location filename="settings.ui" line="254"/>
        <source>אָ֣ז יָשִֽׁיר־מֹשֶׁה֩</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings.ui" line="285"/>
        <source>(Font confs will apply as of next book)</source>
        <translation>(Cette configuration sera appliquée au prochain livre)</translation>
    </message>
    <message>
        <location filename="settings.ui" line="296"/>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <location filename="settings.ui" line="316"/>
        <source>Custom language</source>
        <translation>Langage</translation>
    </message>
    <message>
        <location filename="settings.ui" line="325"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Arial&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:7pt;&quot;&gt;Note: Even when using a custom language, some dialogs may still appear in the system&apos;s language.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settings.ui" line="345"/>
        <source>Use system language</source>
        <translation>Utiliser la langue du système</translation>
    </message>
    <message>
        <location filename="settings.ui" line="380"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="settings.ui" line="400"/>
        <source>Save settings</source>
        <translation>Sauvegarder</translation>
    </message>
</context>
<context>
    <name>bookDisplayer</name>
    <message>
        <source>Add bookmark here...</source>
        <translation type="obsolete">Ajouter un marque-page ici...</translation>
    </message>
    <message>
        <source>Add/edit comment...</source>
        <translation type="obsolete">Ajouter/Editer un commentaire...</translation>
    </message>
    <message>
        <source>Edit comment...</source>
        <translation type="obsolete">Editer le commentaire...</translation>
    </message>
    <message>
        <source>Delete comment</source>
        <translation type="obsolete">Supprimer le commentaire</translation>
    </message>
</context>
<context>
    <name>bookfind</name>
    <message>
        <location filename="bookfind.ui" line="14"/>
        <source>find book by name</source>
        <translation>Trouver un livre d&apos;aprés le titre</translation>
    </message>
    <message>
        <location filename="bookfind.ui" line="26"/>
        <source>Search in name&apos;s beginning</source>
        <translation>Début du titre</translation>
    </message>
    <message>
        <location filename="bookfind.ui" line="36"/>
        <source>Search in the whole name</source>
        <translation>N&apos;importe ou dans le titre</translation>
    </message>
    <message>
        <location filename="bookfind.ui" line="57"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="bookfind.ui" line="67"/>
        <source>Open book</source>
        <translation>Ouvrir le livre</translation>
    </message>
</context>
<context>
    <name>errorReport</name>
    <message>
        <location filename="errorreport.ui" line="14"/>
        <source>Report typo</source>
        <translation>Rapporter une erreur</translation>
    </message>
    <message>
        <location filename="errorreport.ui" line="23"/>
        <source>Typo&apos;s location</source>
        <translation>Position de l&apos;erreur</translation>
    </message>
    <message>
        <location filename="errorreport.ui" line="37"/>
        <source>Describe the typo:</source>
        <translation>Description de l&apos;erreur:</translation>
    </message>
    <message>
        <location filename="errorreport.ui" line="52"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="errorreport.ui" line="72"/>
        <source>Send</source>
        <translation>Envoyer</translation>
    </message>
</context>
<context>
    <name>importBook</name>
    <message>
        <location filename="importbook.ui" line="14"/>
        <source>Import books</source>
        <translation>Importer des livres</translation>
    </message>
    <message>
        <location filename="importbook.ui" line="26"/>
        <source>Books to import:</source>
        <translation>Livres à importer:</translation>
    </message>
    <message>
        <location filename="importbook.ui" line="32"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Arial&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Imported books will be copied to the user&apos;s book folder. In order to import the books for all users, see &amp;quot;&lt;/span&gt;&lt;a href=&quot;http://code.google.com/p/orayta/wiki/AddingNewBooks&quot;&gt;&lt;span style=&quot; font-size:9pt; text-decoration: underline; color:#0057ae;&quot;&gt;Installing new books&amp;quot;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt; In Orayt&apos;a wiki.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Arial&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Imoported books will be copied to the user&apos;s book folder. In order to import the books for all users, see &amp;quot;&lt;/span&gt;&lt;a href=&quot;http://code.google.com/p/orayta/wiki/AddingNewBooks&quot;&gt;&lt;span style=&quot; font-size:9pt; text-decoration: underline; color:#0057ae;&quot;&gt;Installing new books&amp;quot;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt; In Orayt&apos;a wiki.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="importbook.ui" line="79"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="importbook.ui" line="86"/>
        <source>Add folder</source>
        <translation>Ajouter un dossier</translation>
    </message>
    <message>
        <location filename="importbook.ui" line="93"/>
        <source>Add books</source>
        <translation>Ajouter des fichiers</translation>
    </message>
    <message>
        <location filename="importbook.ui" line="130"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="importbook.ui" line="150"/>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <source>Html files(*.htm *.html);;Text files(*.txt);;Pdf files(*.pdf)</source>
        <translation type="obsolete">Fichiers html(*.htm *.html);;Fichiers texte(*.txt);;Fichiers pdf(*.pdf)</translation>
    </message>
    <message>
        <location filename="importbook.cpp" line="58"/>
        <source>All supported files (*.html *.htm *.txt *.pdf);;Html files(*.htm *.html);;Text files(*.txt);;Pdf files(*.pdf)</source>
        <translation>Tous les types de fichiers supportés (*.html *.htm *.txt *.pdf);;Fichiers html (*.htm *.html);;Fichiers texte (*.txt);;Fichiers pdf (*.pdf)</translation>
    </message>
</context>
<context>
    <name>myWebView</name>
    <message>
        <source>Copy text only</source>
        <translation type="obsolete">Coiper le texte uniquement</translation>
    </message>
    <message>
        <source>Copy text</source>
        <translation type="obsolete">Copier le texte</translation>
    </message>
</context>
</TS>
