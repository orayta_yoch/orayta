/* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2
* as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Author: Moshe Wagner. <moshe.wagner@gmail.com>
*/


#include "mainwindow.h"
#include "bookdisplayer.h"
#include "treeitem_orayta.h"

#if defined(_MSC_VER) && _MSC_VER >= 1500
# include <functional>
#else
# include <tr1/functional>
#endif

#include <QtConcurrentMap>

#define QT_USE_FAST_CONCATENATION
#define QT_USE_FAST_OPERATOR_PLUS

// This is just a very simple define. every place in the code,
//  "CURRENT_TAB" simply represents "ui->viewTab->currentIndex()".
#define CURRENT_TAB ui->viewTab->currentIndex()



static QString createSearchPattern (QString userInput, bool allWords = true, bool fullWords = false, int spacing = 0)
{
    QStringList words = userInput.split(" ", QString::SkipEmptyParts);
    QString pattern;

    if (!fullWords)
    {
        for (int i=0; i < words.size(); i++)
            words[i] = "[^ ]*" + words[i] + "[^ ]*";
    }
    else if (!allWords) // && fullWords
    {
        for (int i=0; i < words.size(); i++)
            words[i] = " " + words[i] + " ";
    }

    if (allWords)
    {
        QString sep = (spacing == 0 ? " " : " ([א-ת]+ ){0," + QString::number(spacing) + "}");
        pattern = words.join(sep);

        if (fullWords)
            pattern = " " + pattern + " ";
    }
    else
    {
        pattern = "(" + words.join(")|(") + ")";
    }

    return pattern;
}

void MainWindow::on_searchInBooksLine_returnPressed()
{
    on_SearchInBooksBTN_clicked();
}

void MainWindow::on_SearchInBooksBTN_clicked()
{
    QString otxt = ui->searchInBooksLine->text();
    QString stxt = otxt;
    QRegExp regexp;

    if (otxt == "" || otxt == " ")
        return;

    if (ui->guematriaCheckBox->isChecked())
    {
        SearchGuematriaInTanach (otxt);
    }
    else
    {
        if (ui->fuzzyCheckBox->isChecked())
            stxt = AllowKtivHasser(stxt);

        bool allwords = ui->radioButton_2->isChecked();
        bool fullwords = ui->fullCheckBox->isChecked();
        int spacing = ui->spinBox->value();

        if (ui->radioButton_3->isChecked())
            regexp = QRegExp(stxt);
        else
            regexp = QRegExp( createSearchPattern (stxt, allwords, fullwords, spacing) );
        regexp.setMinimal(true);

        SearchInSelectedBooks(regexp, otxt);
    }
}

void MainWindow::on_groupBox_toggled(bool checked)
{
    if (checked)
    {
        ui->guematriaCheckBox->setChecked(false);
    }
}

void MainWindow::on_guematriaCheckBox_toggled(bool checked)
{
    if (checked)
    {
        ui->groupBox->setChecked (false);
    }
}

void MainWindow::on_radioButton_2_toggled(bool checked)
{
    if (checked)
        ui->spinBox->setEnabled (true);
    else
        ui->spinBox->setEnabled (false);
}

void MainWindow::on_radioButton_3_toggled(bool checked)
{
    if (checked)
        ui->fullCheckBox->setEnabled (false);
    else
        ui->fullCheckBox->setEnabled (true);
}


static QPair < QString, QList<SearchResult> > findInOryBook(const NodeBook* book, const QRegExp& regexp)
{
    //qDebug() << "search in " << book->getTreeDisplayName() << " for " << regexp.pattern();
    /// WARNING : QRegExp is NOT thread safe, so make copy !!!
    return qMakePair(book->getTreeDisplayName(), dynamic_cast<const OraytaBookItem*>(book)->findInBook( QRegExp(regexp) ));
}


void MainWindow::SearchInSelectedBooks (const QRegExp& regexp, QString disp)
{
    //TODO: make preview look nice
    // ######### A ré-étudier : le type de books rencoyés par cette fonction devrait absolument avoir une methode search
    if ( regexp.pattern() != "" )
    {
        BookList searchList = ui->treeWidget->BooksInSearch();

        // Start the computation.
        watcher.setMaximum(MAX_RESULTS);
        watcher.addEndAction(std::tr1::bind(&MainWindow::displaySearchResults, this, disp));

        searchFuture = QtConcurrent::mapped( searchList, std::tr1::bind(findInOryBook, std::tr1::placeholders::_1, regexp) );
        watcher.setFuture(searchFuture);
    }
}

void MainWindow::displaySearchResults(QString searchedWord)
{
    BookDisplayer* pCurrentBookdisplayer = addViewTab();

    QString title, Html="", Htmlhead="", HtmlTopLinks="";

    //Head and title of the Html
    title = tr("Search results: ") + "\"" + searchedWord + "\"";

    Htmlhead = html_head(title, gFont.family() ,gFont.pointSize());
    Htmlhead += "<body><div class=\"Section1\" dir=\"RTL\">";
    Htmlhead += "<div style=\"font-size:30px\"><b><i><center>";
    Htmlhead += title + ":" + "</center></i></b></div><BR>";
    Htmlhead += "\n<span style=\"font-size:17px\">\n";

    /// For security !!!
    searchFuture.waitForFinished();

    int results = 0, totalResults = 0, nBooksInResults = 0;
    for (QFuture< QPair < QString, QList<SearchResult> > >::const_iterator it = searchFuture.begin();
         it != searchFuture.end() && results < MAX_RESULTS;
         ++it)
    {
        //Let the animation move...
        QApplication::processEvents();

        const QString bookname = (*it).first;
        const QList <SearchResult> searchResults = (*it).second;

        if (searchResults.empty()) continue;

        nBooksInResults++;

        //Add a small link (at the index) to the full result
        HtmlTopLinks += reddot() + "&nbsp;&nbsp;<a onclick=\"paintWhoILinkTo(this)\" href=\"#" + QString::number(results + 1) + "\">";

        int resultsInThisBook = 0;
        for (int i=0; i < searchResults.size() && results < MAX_RESULTS; i++)
        {
            results++;
            resultsInThisBook += searchResults[i].nbResults;

            //Add the full result to the page
            Html += "<span style=\"font-size:23px\"><a id=\"id_" + QString::number(results)+ "\" name=\"" + QString::number(results) + "\"";
            Html += " href=\"" + searchResults[i].link + "\">";
            Html += QString::number(results) + ")&nbsp;" + searchResults[i].linkdisplay;
            if (searchResults[i].nbResults > 1)
				Html += "&nbsp;<small><i>[" + QString::number(searchResults[i].nbResults) + tr(" occurences") +"]</i></small>";
            Html += "</a></span><BR>\n";

            //Show result
            Html += searchResults[i].preview;
            Html += "<br><br><br>\n";
        }
        totalResults += resultsInThisBook;

        HtmlTopLinks += bookname + "&nbsp;&nbsp;<small>(" + QString::number(resultsInThisBook) + tr(" results") + ")</small></a><BR>\n";
    }

    if (watcher.isUserCanceled())
    {
        Htmlhead += "<BR><BR>" + tr("(Search stopped by user)") + "<BR><BR>";
    }

    if (results == 0)
    {
        //TODO: write better explenation
        Htmlhead +="<BR><BR>" + tr("No search results found:");
        Htmlhead += "\"" + searchedWord + "\"</B><BR>";
    }
    else
    {
        Htmlhead += "<B>" + tr("Short result list: ") + "</B><BR>";
        if (watcher.maximumExeeded())
        {
            Htmlhead += "(" + QString::number(totalResults) + tr(" firsts results only") + ")<BR><BR>";
        }
        else
        {
            Htmlhead += "(" + QString::number(totalResults) + tr(" entries founds") + ") <BR><BR>";
        }

        Htmlhead += HtmlTopLinks;

        Htmlhead += "<BR><BR><B>" +  tr("Full result list:") + "</B><BR><BR>";
    }

    Html = Htmlhead + Html;

    Html += "</span></div>\n";
    Html += "\n</body>\n</html>";

    searchFuture.results().clear();

    //TODO: Search results are special books
    // ########### On pourrait ajouter les pages de recherches à l'arbre, par exemple...
    writetofile(TMPPATH + "Search.html", Html, "UTF-8");

    pCurrentBookdisplayer->loadSearchPage(QUrl::fromLocalFile(TMPPATH + "Search.html"));
}

static QList<OraytaBookItem*> getBooksChildren( BaseNodeItem* parent )
{
    QList<OraytaBookItem*> ret;

    for ( QList<BaseNodeItem*>::const_iterator it = parent->getChildren().begin(); it != parent->getChildren().end(); ++it )
    {
        if ( (*it)->nodetype() == BaseNodeItem::Leaf )
        {
            OraytaBookItem* obook = dynamic_cast<OraytaBookItem*>(*it);
            if (obook && obook->IsInSearch()) ret << obook;
        }
        else
            ret << getBooksChildren( *it );
    }

    return ret;
}

static QPair < QString, QList<SearchResult> > findGuematriaInTanachBook(const OraytaBookItem* obook, int value)
{
    return qMakePair( obook->getTreeDisplayName(), obook->findGuematria(value) );
}

void MainWindow::SearchGuematriaInTanach( const QString& txt )
{
    int guematria = GematriaValueOfString ( txt );
    if ( guematria == 0 )
        return;

    BaseNodeItem* tanachRoot = ui->treeWidget->tanachRoot();
    if (tanachRoot == NULL)
    {
        qDebug("tanach root not found !");
        return;
    }

    QList<OraytaBookItem*> tanachList = getBooksChildren( tanachRoot );

    // Start the computation.
    watcher.setMaximum(-1);
    watcher.addEndAction(std::tr1::bind(&MainWindow::displayGuematriaSearchResults, this, txt, guematria));

    searchFuture = QtConcurrent::mapped( tanachList, std::tr1::bind(findGuematriaInTanachBook, std::tr1::placeholders::_1, guematria) );
    watcher.setFuture(searchFuture);
}

void MainWindow::displayGuematriaSearchResults(QString txt, int guematria)
{
    QString title = "", Htmlhead = "", Html = "";

    BookDisplayer* pCurrentBookdisplayer = addViewTab();

    /// For security !!!
    searchFuture.waitForFinished();

    int nb_results = 0, nbBooksInSearch = 0;
    for (QFuture< QPair < QString, QList<SearchResult> > >::const_iterator  it = searchFuture.begin();
                                                                            it != searchFuture.end();
                                                                            ++it)
    {
        //Let the animation move...
        QApplication::processEvents();

        nbBooksInSearch++;  // ??

        //const QString bookname = (*it).first;
        const QList <SearchResult> results = (*it).second;

        for (int j=0; j < results.size(); j++)
        {
            nb_results++;

            //Add the full result to the page
            Html += "<span style=\"font-size:20px\">";
            Html += "<a href=\"" + results[j].link + "\">";
            Html += QString::number(nb_results) + ") " + results[j].linkdisplay;
            Html += "</a><br></span>\n";
            Html += results[j].preview;
            Html += "<br><br>\n";
        }
    }

    //Head and title of the Html
    title = tr("Search results for guematria : ") + "\"" + txt + "\"";
    title += " (" + QString::number(guematria) + ")";

    Htmlhead = html_head(title, gFont.family(), gFont.pointSize());
    Htmlhead += "<body><div class=\"Section1\" dir=\"RTL\">";
    Htmlhead += "<div style=\"font-size:30px\"><b><i><center>";
    Htmlhead += title + ":" + "</center></i></b></div><BR>";
    Htmlhead += "\n<span style=\"font-size:17px\">";

    if (watcher.isUserCanceled())
    {
        Htmlhead += "<BR><BR>" + tr("(Search stopped by user)") + "<BR><BR>";
    }

    if (nbBooksInSearch == 0)
    {
        Htmlhead +="<br><br><center><b>";
        Htmlhead += tr("No tanach books selected : please select books in tanach and search again.");
        Htmlhead += "</b></center><br>";
    }
    else if (nb_results == 0)
    {
        Htmlhead +="<br><b>";
        Htmlhead += tr("No guematria results found:");
        Htmlhead += "\"" + txt + "\"";
        Htmlhead += "</b><br>";
    }
    else
    {
        Htmlhead += "<b>";
        Htmlhead += tr("Result list: ");
        Htmlhead += "</b> "+ QString::number(nb_results) + tr(" results founds.") + "<br><br>";
    }

    Html = Htmlhead + Html;

    Html += "</span></div>\n";
    Html += "\n</body>\n</html>";

    writetofile(TMPPATH + "Guematria.html", Html, "UTF-8");

    pCurrentBookdisplayer->loadSearchPage(QUrl::fromLocalFile(TMPPATH + "Guematria.html"));
}
