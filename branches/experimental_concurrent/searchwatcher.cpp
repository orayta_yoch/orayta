#include "searchwatcher.h"

SearchWatcher::SearchWatcher(int max) :
    nbResultsMax(max),
    nbResults(0)
{
    connect(this, SIGNAL(started()), this, SLOT(reset()));
    connect(this, SIGNAL(resultReadyAt(int)), this, SLOT(updateNbResultsAt(int)));
    connect(this, SIGNAL(finished()), this, SLOT(endActions()));
}


void SearchWatcher::setMaximum(int max)
{
    reset();
    nbResultsMax = max;
}

void SearchWatcher::addEndAction(fn_obj endFunc)
{
    endFunctions << endFunc;
}

bool SearchWatcher::maximumExeeded() const
{
    if (nbResultsMax == -1)  // -1 is INF
        return false;
    return nbResults > nbResultsMax;
}

bool SearchWatcher::isUserCanceled() const
{
    if (!maximumExeeded())
        return isCanceled();
    return false;
}

void SearchWatcher::updateNbResultsAt(int index)
{
    nbResults += resultAt(index).second.size();

    if (maximumExeeded())
    {
        qDebug() << "  max results exeeded : " << nbResults << "results";
        cancel();
    }
}

void SearchWatcher::endActions()
{
    while (!endFunctions.empty())
    {
        endFunctions.takeFirst()();
    }
    qDebug() << "  elapsed : " << tm.elapsed() << "ms";
}

void SearchWatcher::reset()
{
    nbResults = 0;
    tm.start();
}
