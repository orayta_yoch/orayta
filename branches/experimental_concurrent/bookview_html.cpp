
#include "bookview_html.h"

#include <QDebug>


HtmlBookView::HtmlBookView( BookDisplayer * parent ) :
    BaseHtmlView(parent),
    mAdditionalButtons(new QWidget(this)),
    mAdditionalLayout(new QHBoxLayout),
    backBtn(new QToolButton(this)),
    frwdBtn(new QToolButton(this))
{
    backAction = pageAction(QWebPage::Back);
    backAction->setShortcut(QKeySequence::Back);
    forwardAction = pageAction(QWebPage::Forward);
    forwardAction->setShortcut(QKeySequence::Forward);
    reloadAction = pageAction(QWebPage::Reload);
    reloadAction->setShortcut(QKeySequence::Refresh);

    addAction(backAction);
    addAction(forwardAction);
    addAction(reloadAction);

    backBtn->setDefaultAction(backAction);
    backBtn->setAutoRaise(true);
    frwdBtn->setDefaultAction(forwardAction);
    frwdBtn->setAutoRaise(true);

    mAdditionalLayout->setContentsMargins(0,0,0,0);
    mAdditionalLayout->setDirection(QBoxLayout::RightToLeft);
    mAdditionalLayout->addWidget(backBtn);
    mAdditionalLayout->addWidget(frwdBtn);
    mAdditionalButtons->setLayout(mAdditionalLayout);

    connect (this, SIGNAL(urlChanged(QUrl)), this, SLOT(onUrlChanged(QUrl)));
}

NodeBook::Booktype HtmlBookView::booktype() const
{  return NodeBook::Html;  }

/* ##########
A partir du moment ou l'on clique sur un lien externe, il faut impérativement supprimer l'association entre
le BookDisplayer et le NodeBook, sinon on ne peut plus ouvrir le livre par clic sur le TreeWidget.
--- fait pour l'instant exterieurement ---
*/
void HtmlBookView::loadBook(const NodeBook* book)
{
    if (book->booktype() != NodeBook::Html) return;  // sanity check

    loadUrl(QUrl::fromLocalFile(book->getLoadableFile()));
}

void HtmlBookView::onUrlChanged(QUrl url)
{
    qDebug() << " url changed to : " << url.toString();
}

/*
void HtmlBookView::reload()
{
    page()->triggerAction(QWebPage::Reload);
}
*/

QWidget* HtmlBookView::additionalButtons()
{  return mAdditionalButtons;  }
