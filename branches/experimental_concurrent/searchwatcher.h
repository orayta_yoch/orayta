#ifndef SEARCHWATCHER_H
#define SEARCHWATCHER_H

#include <QObject>
#include <QFutureWatcher>
#include <QTime>

#if defined(_MSC_VER) && _MSC_VER >= 1500
# include <functional>
#else
# include <tr1/functional>
#endif


#include "searchresults.h"
#include "settings.h"

typedef std::tr1::function< void(void) > fn_obj;

class SearchWatcher : public QFutureWatcher< QPair < QString, QList<SearchResult> > >
{
    Q_OBJECT
public:
    SearchWatcher(int max = MAX_RESULTS);

    void addEndAction(fn_obj);
    void setMaximum(int);
    bool maximumExeeded() const;
    bool isUserCanceled() const;

private slots:

    void updateNbResultsAt(int);
    void endActions();
    void reset();

private:
    int nbResultsMax;
    int nbResults;
    QList<fn_obj> endFunctions;
    QTime tm;
};

#endif // SEARCHWATCHER_H
