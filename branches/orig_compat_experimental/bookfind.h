/* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2
* as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Author: Moshe Wagner. <moshe.wagner@gmail.com>
*/

#ifndef BOOKFIND_H
#define BOOKFIND_H

#include <QDialog>
#include "functions.h"
#include "mainwindow.h"

namespace Ui {
    class bookfind;
}

class bookfind : public QDialog {
    Q_OBJECT
public:
    bookfind(QWidget *parent, const BookList& booklist);
    ~bookfind();

protected:

private:
    Ui::bookfind *m_ui;
    BookList mBookList;

    void toRTL();
    void reBuildList(const QString&);

signals:
    void openBookRequested(NodeBook*);

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_radioButton_2_toggled(bool checked);
    void on_lineEdit_textEdited(const QString&);
    void on_listWidget_itemDoubleClicked(QListWidgetItem *);
};

#endif // BOOKFIND_H
