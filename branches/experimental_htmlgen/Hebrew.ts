<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="he_IL" sourcelanguage="en">
<context>
    <name></name>
    <message>
        <source>All right reserved </source>
        <translation type="obsolete">כל הזכויות שמורות</translation>
    </message>
    <message>
        <source>Open book</source>
        <translation type="obsolete">פתח ספר</translation>
    </message>
    <message>
        <source>Open in new tab</source>
        <translation type="obsolete">פתח בלשונית חדשה</translation>
    </message>
    <message>
        <source>Edit bookmark title...</source>
        <translation type="obsolete">ערוך כותרת סימנייה...</translation>
    </message>
    <message>
        <source>Delete bookmark</source>
        <translation type="obsolete">מחק סימנייה</translation>
    </message>
    <message>
        <source>Orayta</source>
        <translation type="obsolete">אורייתא</translation>
    </message>
</context>
<context>
    <name>About</name>
    <message>
        <location filename="about.ui" line="14"/>
        <source>About Orayta</source>
        <translation>אודות אורייתא</translation>
    </message>
    <message>
        <location filename="about.cpp" line="26"/>
        <source>Orayta - Hebrew Books</source>
        <oldsource>&lt;center&gt;&lt;b&gt;Orayta - Hebrew Books</oldsource>
        <translation>אורייתא - ספרי קודש</translation>
    </message>
    <message>
        <location filename="about.cpp" line="41"/>
        <location filename="about.cpp" line="42"/>
        <source>5770</source>
        <translation>תש&quot;ע</translation>
    </message>
    <message>
        <location filename="about.cpp" line="43"/>
        <source>Torat Emet software</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>   Moshe Wagner - </source>
        <translation type="obsolete">משה וגנר -</translation>
    </message>
    <message>
        <source>   moshe.wagner@gmail.com, 5770</source>
        <translation type="obsolete">   moshe.wagner@gmail.com, תש&quot;ע</translation>
    </message>
</context>
<context>
    <name>AddBookMark</name>
    <message>
        <location filename="addbookmark.ui" line="17"/>
        <source>Bookmark&apos;s title</source>
        <translation>כותרת הסימנייה</translation>
    </message>
    <message>
        <location filename="addbookmark.ui" line="26"/>
        <source>Bookmark&apos;s title:</source>
        <translation>כותרת הסימנייה:</translation>
    </message>
    <message>
        <location filename="addbookmark.ui" line="42"/>
        <source>Cancel</source>
        <translation>ביטול</translation>
    </message>
    <message>
        <location filename="addbookmark.ui" line="62"/>
        <source>OK</source>
        <translation>אישור</translation>
    </message>
</context>
<context>
    <name>AddComment</name>
    <message>
        <location filename="addcomment.ui" line="17"/>
        <source>Add comment</source>
        <translation>הוסף/ערוך הערה</translation>
    </message>
    <message>
        <location filename="addcomment.ui" line="71"/>
        <source>Save</source>
        <translation>שמור</translation>
    </message>
    <message>
        <location filename="addcomment.ui" line="64"/>
        <source>Cancel</source>
        <translation>ביטול</translation>
    </message>
    <message>
        <location filename="addcomment.ui" line="41"/>
        <source>Delete comment</source>
        <translation>מחק הערה</translation>
    </message>
</context>
<context>
    <name>BaseHtmlView</name>
    <message>
        <location filename="bookview_html.cpp" line="25"/>
        <source>Copy text</source>
        <translation>העתק טקסט</translation>
    </message>
    <message>
        <location filename="bookview_html.cpp" line="178"/>
        <source>404 - Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="bookview_html.cpp" line="178"/>
        <source>file not found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BookMarkTitle</name>
    <message>
        <source>Bookmark&apos;s title</source>
        <translation type="obsolete">כותרת הסימנייה</translation>
    </message>
    <message>
        <source>Bookmark&apos;s title:</source>
        <translation type="obsolete">כותרת הסימנייה:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">ביטול</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">אישור</translation>
    </message>
</context>
<context>
    <name>BookTree</name>
    <message>
        <location filename="booktree.cpp" line="83"/>
        <source>Orayta Library</source>
        <translation>ספריית אורייתא</translation>
    </message>
    <message>
        <location filename="booktree.cpp" line="84"/>
        <source>User Library</source>
        <translation>ספריית המשתמש</translation>
    </message>
    <message>
        <location filename="booktree.cpp" line="91"/>
        <source>Open book</source>
        <translation>פתח ספר</translation>
    </message>
    <message>
        <location filename="booktree.cpp" line="92"/>
        <source>Open in new tab</source>
        <translation>פתח בלשונית חדשה</translation>
    </message>
    <message>
        <location filename="booktree.cpp" line="93"/>
        <source>Delete book</source>
        <translation>מחק ספר זה</translation>
    </message>
    <message>
        <location filename="booktree.cpp" line="94"/>
        <source>Change font</source>
        <translation>שנה גופן</translation>
    </message>
    <message>
        <location filename="booktree.cpp" line="374"/>
        <source>Are you sure you want to remove this </source>
        <translation>האם אתה בטוח שברצונך למחוק </translation>
    </message>
    <message>
        <location filename="booktree.cpp" line="375"/>
        <source>books directory ?
This will remove all the books in this directory.</source>
        <translation>תיקיה זו ?
כל הספרים הקיימים בתקיה יימחקו.</translation>
    </message>
    <message>
        <location filename="booktree.cpp" line="376"/>
        <source>book ?</source>
        <translation>ספר זה?</translation>
    </message>
    <message>
        <location filename="booktree.cpp" line="378"/>
        <source>Deleting book</source>
        <translation>מחיקה</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="100"/>
        <location filename="mainwindow.cpp" line="391"/>
        <source>Orayta</source>
        <translation>אורייתא</translation>
    </message>
    <message>
        <source>No books found! 
Check your installation, or contact the developer.</source>
        <translation type="obsolete">לא נמצאו ספרים מותקנים! 
 בדוק שהתוכנה הותקנה כראוי, ופנה למפתח.התוכנה.</translation>
    </message>
    <message>
        <source>Jewish books</source>
        <translation type="obsolete">ספרי קודש</translation>
    </message>
    <message>
        <source>Loading...</source>
        <translation type="obsolete">טוען...</translation>
    </message>
    <message>
        <source>Are you sure you want to remove this </source>
        <translation type="obsolete">אתה בטוח שברצונך למחוק </translation>
    </message>
    <message>
        <source>books directory ?
This will remove all the books in this directory.</source>
        <translation type="obsolete">תיקיה זו ?
כל הספרים הקיימים בתקיה יימחקו.</translation>
    </message>
    <message>
        <source>book ?</source>
        <translation type="obsolete">ספר זה?</translation>
    </message>
    <message>
        <source>Deleting book</source>
        <translation type="obsolete">מחיקת ספר</translation>
    </message>
    <message>
        <source>Delete book</source>
        <translation type="obsolete">מחק ספר זה</translation>
    </message>
    <message>
        <source>Change font</source>
        <translation type="obsolete">שנה גופן</translation>
    </message>
    <message>
        <source>Page: </source>
        <translation type="obsolete">עמוד: </translation>
    </message>
    <message>
        <source>Add bookmark here...</source>
        <translation type="obsolete">הוסף סימנייה כאן...</translation>
    </message>
    <message>
        <source>Add/edit comment...</source>
        <translation type="obsolete">הוסף/ערוך הערה...</translation>
    </message>
    <message>
        <source>Edit comment...</source>
        <translation type="obsolete">ערוך הערה...</translation>
    </message>
    <message>
        <source>Delete comment</source>
        <translation type="obsolete">מחק הערה</translation>
    </message>
    <message>
        <source>RegExp: </source>
        <translation type="obsolete">ביטוי רגולרי:</translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="161"/>
        <source>Searching: </source>
        <translation>מחפש:</translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="165"/>
        <source>Search results: </source>
        <translation>תוצאות חיפוש :</translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="210"/>
        <location filename="mainwnd_search.cpp" line="341"/>
        <source>(Search stopped by user)</source>
        <translation>(החיפוש נעצר ע&quot;י המשתמש)</translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="217"/>
        <source>No search results found:</source>
        <translation>לא נמצאו תוצאות לחיפוש המבוקש:</translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="225"/>
        <source>firsts results only</source>
        <translation>תוצאות ראשונות בלבד</translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="229"/>
        <source> entries founds</source>
        <translation> תוצאות נמצאו</translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="297"/>
        <source>Searching guematria for: </source>
        <translation>חיפוש גימטריא עבור: </translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="330"/>
        <source>Search results for guematria : </source>
        <translation>תוצאות חיפוש גימטריא עבור :</translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="348"/>
        <source>No tanach books selected : please select books in tanach and search again.</source>
        <oldsource>Any tanach books selected : please select books in tanach and search again.</oldsource>
        <translation>ספרי תנ&quot;ך לא נבחרו : נא בחר הספרים מהתנ&quot;ך בהם ברצונך לבצע החיפוש.</translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="354"/>
        <source>No guematria results found:</source>
        <translation>לא נמצאו תוצאות:</translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="361"/>
        <source>Result list: </source>
        <translation>רשימת תוצאות: </translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="362"/>
        <source> results founds.</source>
        <oldsource> results founds.&lt;br&gt;&lt;br&gt;</oldsource>
        <translation> תוצאות נמצאו.</translation>
    </message>
    <message>
        <source>No serach results found:</source>
        <translation type="obsolete">לא נמצאו תוצאות לחיפוש המבוקש:</translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="222"/>
        <source>Short result list: </source>
        <translation>רשימת תוצאות מקוצרת:</translation>
    </message>
    <message>
        <location filename="mainwnd_search.cpp" line="233"/>
        <source>Full result list:</source>
        <oldsource>full result list</oldsource>
        <translation>רשימת תוצאות מלאה:</translation>
    </message>
    <message>
        <source>Open book</source>
        <translation type="obsolete">פתח ספר</translation>
    </message>
    <message>
        <source>Open in new tab</source>
        <translation type="obsolete">פתח בלשונית חדשה</translation>
    </message>
    <message>
        <location filename="mainwnd_bookmark.cpp" line="26"/>
        <source>Edit bookmark title...</source>
        <translation>ערוך כותרת סימנייה...</translation>
    </message>
    <message>
        <location filename="mainwnd_bookmark.cpp" line="27"/>
        <source>Delete bookmark</source>
        <translation>מחק סימנייה</translation>
    </message>
    <message>
        <source>Orayta - Jewish books</source>
        <translation type="obsolete">אורייתא - ספרי קודש</translation>
    </message>
</context>
<context>
    <name>MainWindowClass</name>
    <message>
        <location filename="mainwindow.ui" line="681"/>
        <location filename="mainwindow.ui" line="876"/>
        <location filename="mainwindow.ui" line="890"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="26"/>
        <source>Orayta - Hebrew books</source>
        <translation>אורייתא - ספרי קודש</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="502"/>
        <source>Orayta</source>
        <translation>אורייתא</translation>
    </message>
    <message>
        <source>Search:</source>
        <translation type="obsolete">חפש:</translation>
    </message>
    <message>
        <source>Search in selected books</source>
        <translation type="obsolete">חפש בספרים המסומנים</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1050"/>
        <location filename="mainwindow.ui" line="1261"/>
        <source>Search</source>
        <translation>חפש</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="725"/>
        <source>Search backwards in book</source>
        <translation>חפש אחורה בספר זה</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="754"/>
        <source>Search forward in book</source>
        <translation>חפש קדימה בספר זה</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="560"/>
        <source>Zoom out</source>
        <translation>הקטן כתב</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="589"/>
        <source>Zoom in</source>
        <translation>הגדל כתב</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="531"/>
        <source>Jump to top</source>
        <translation>קפוץ לראש הספר</translation>
    </message>
    <message>
        <source>Pdf page:</source>
        <translation type="obsolete">עמוד:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="618"/>
        <source>Search in current books</source>
        <translation>חפש בתוך ספר זה</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="711"/>
        <source>Search in book:</source>
        <translation>חיפוש בספר:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="845"/>
        <source>Books</source>
        <translation>ספרים</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="873"/>
        <source>Add all books to search</source>
        <oldsource>Add all bokks to search</oldsource>
        <translation>הוסף את כל הספרים לחיפוש</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="887"/>
        <source>Remove all books from search</source>
        <translation>הוצא את כל הספרים מהחיפוש</translation>
    </message>
    <message>
        <source>showAlone</source>
        <translation type="obsolete">הצג לבד</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1392"/>
        <source>Bookmarks</source>
        <translation>סימניות</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1406"/>
        <source>Erase bookmark</source>
        <translation>מחק סימנייה</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1432"/>
        <source>Move bookmark up</source>
        <translation>העלה סימנייה למעלה</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1461"/>
        <source>Move bookmark down</source>
        <translation>הורד סימנייה למטה</translation>
    </message>
    <message>
        <source>Configure</source>
        <translation type="obsolete">הגדרות</translation>
    </message>
    <message>
        <source>Default font:</source>
        <translation type="obsolete">פונט ברירת מחדל:</translation>
    </message>
    <message>
        <source>Base font size:</source>
        <translation type="obsolete">גודל פונט בסיס:</translation>
    </message>
    <message>
        <source>font preview:</source>
        <translation type="obsolete">תצוגה מקדימה:</translation>
    </message>
    <message>
        <source>(Make sure the font can handle Nikud and Teamim)</source>
        <translation type="obsolete">(מומלץ לוודא שהפונט תומך בניקוד וטעמים)</translation>
    </message>
    <message>
        <source>Save changes</source>
        <translation type="obsolete">שמור שינויים</translation>
    </message>
    <message>
        <source>(Confs will apply as of next book)</source>
        <translation type="obsolete">(ההגדרות יחולו החל מהספרים הבאים)</translation>
    </message>
    <message>
        <source>Gui language:</source>
        <translation type="obsolete">שפת הממשק:</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="obsolete">English</translation>
    </message>
    <message utf8="true">
        <source>עברית</source>
        <translation type="obsolete">עברית</translation>
    </message>
    <message>
        <source>Changing the language will restart the application, and close all open books</source>
        <translation type="obsolete">שינוי השפה יגרום לאתחול התוכנה, ולסגירת כל הספרים הפתוחים</translation>
    </message>
    <message>
        <source>Change language</source>
        <translation type="obsolete">שנה שפה</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1665"/>
        <source>&amp;File</source>
        <oldsource>File</oldsource>
        <translation>&amp;קובץ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1680"/>
        <source>&amp;Help</source>
        <oldsource>Help</oldsource>
        <translation>&amp;עזרה</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1689"/>
        <source>&amp;Display</source>
        <oldsource>Display</oldsource>
        <translation>&amp;תצוגה</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1724"/>
        <source>&amp;Location</source>
        <oldsource>Location</oldsource>
        <translation>&amp;מיקום נוכחי</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1742"/>
        <source>E&amp;xit</source>
        <oldsource>Exit</oldsource>
        <translation>י&amp;ציאה</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1751"/>
        <source>&amp;About</source>
        <oldsource>About</oldsource>
        <translation>&amp;אודות</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1787"/>
        <source>&amp;Print book</source>
        <oldsource>Print book</oldsource>
        <translation>הד&amp;פס ספר</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1796"/>
        <source>&amp;Close tab</source>
        <oldsource>Close tab</oldsource>
        <translation>&amp;סגור לשונית</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="508"/>
        <source>Open new tab</source>
        <translation>פתח לשונית חדשה</translation>
    </message>
    <message>
        <source>Find exact string</source>
        <translation type="obsolete">חפש משפט שלם</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1150"/>
        <source>spacing (in words)</source>
        <translation>רווח בין המילים</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1191"/>
        <source>Allow &quot;Ktiv Haser&quot;</source>
        <translation>חפש גם בכתיב חסר</translation>
    </message>
    <message>
        <source>Regular expression search</source>
        <translation type="obsolete">חיפוש ביטוי רגולרי</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1209"/>
        <source>Guematria search (tanach only)</source>
        <translation>חיפוש גימטריא - תנ&quot;ך בלבד</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1403"/>
        <source>Remove bookmark</source>
        <translation>מחק סימנייה</translation>
    </message>
    <message>
        <source>Display alone</source>
        <translation type="obsolete">הצג לבד</translation>
    </message>
    <message>
        <source>Display with:</source>
        <translation type="obsolete">הצג בשילוב עם:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="996"/>
        <source>Hide commentaries</source>
        <translation>הסתר פרושים</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1018"/>
        <source>Open</source>
        <translation>הצג</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1061"/>
        <source>Search for:</source>
        <translation>חפש:</translation>
    </message>
    <message>
        <source>( The search ignores Punctuation, Nikud and Teamim. )
</source>
        <translation type="obsolete">( החיפוש מתעלם מפיסוק, ניקוד וטעמים )</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1093"/>
        <source>String search</source>
        <translation>חיפוש מחרוזות</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1162"/>
        <source>Find any of the words</source>
        <translation>חפש חלק מהמילים</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1131"/>
        <source>Find all words</source>
        <translation>חפש את כל המילים</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1179"/>
        <source>Find full words only</source>
        <translation>חפש מילים שלמות</translation>
    </message>
    <message>
        <source>Allow &quot;Ktiv Maleh&quot; / &quot;Ktiv Haser&quot;</source>
        <translation type="obsolete">חפש בכתיב מלא/חסר</translation>
    </message>
    <message>
        <source>Find regular expression</source>
        <translation type="obsolete">חפש ביטוי רגולרי</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1302"/>
        <source>Searching ...</source>
        <translation>מחפש ...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1349"/>
        <source>Cancel search</source>
        <translation>בטל חיפוש</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1816"/>
        <source>Show nikud</source>
        <translation>הצג ניקוד</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1827"/>
        <source>Show teamim</source>
        <translation>הצג טעמים</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1630"/>
        <source>Add bookmark</source>
        <translation>הוסף סימנייה</translation>
    </message>
    <message>
        <source>refresh this book</source>
        <translation type="obsolete">רענן ספר זה</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1110"/>
        <source>Find exact string (or regexp)</source>
        <translation>חפש משפט מדיוק
- או ביטוי רגולרי</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1584"/>
        <source>Add a bookmark here:</source>
        <translation>הוסף סימניה כאן:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1601"/>
        <source>Title:</source>
        <translation>כותרת:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1702"/>
        <source>&amp;Search</source>
        <translation>&amp;חיפוש</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1760"/>
        <source>Zoom &amp;in</source>
        <translation>ה&amp;גדל כתב</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1769"/>
        <source>Zoom &amp;out</source>
        <translation>ה&amp;קטן כתב</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1778"/>
        <source>&amp;Jump to top</source>
        <translation>קפוץ ל&amp;ראש הספר</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1805"/>
        <source>&amp;Open new tab</source>
        <translation>פתח ב&amp;כרטסיה חדשה</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1836"/>
        <source>Add &amp;bookmark</source>
        <translation>הוסף &amp;סימניה</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1845"/>
        <source>Add/edit &amp;comment</source>
        <oldsource>Add/edit comment</oldsource>
        <translation>הוסף/ערוך ה&amp;ערה</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1854"/>
        <source>&amp;Report typo</source>
        <oldsource>Report typo</oldsource>
        <translation>&amp;דווח על טעות</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1863"/>
        <source>&amp;Search in books</source>
        <oldsource>Search in books</oldsource>
        <translation>&amp;חיפוש בספרים</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1872"/>
        <source>Search in current book</source>
        <translation>חפש בספר הנוכחי</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1944"/>
        <source>&amp;Settings</source>
        <oldsource>Settings</oldsource>
        <translation>ה&amp;גדרות</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1953"/>
        <source>&amp;Import</source>
        <oldsource>Import</oldsource>
        <translation>&amp;יבוא ספרים</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1962"/>
        <source>Search &amp;guematria</source>
        <oldsource>Search guematria</oldsource>
        <translation>חיפוש &amp;גימטריא</translation>
    </message>
    <message>
        <source>Advanced search</source>
        <translation type="obsolete">חיפוש מתקדם</translation>
    </message>
    <message>
        <source>Search in book</source>
        <translation type="obsolete">חפש בספר</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1881"/>
        <source>Search forward</source>
        <translation>חפש קדימה</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1890"/>
        <source>Search backwards</source>
        <translation>חפש אחורה</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1899"/>
        <source>Remove book from search</source>
        <translation>הוצא מהחיפוש</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1908"/>
        <source>Add book to search</source>
        <translation>הוסף לחיפוש</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1917"/>
        <source>Add all to search</source>
        <translation>הוסף הכל לחיפוש</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1926"/>
        <source>Remove all from search</source>
        <translation>הוצא הכל מהחיפוש</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1935"/>
        <source>&amp;Find book by name</source>
        <oldsource>Find book by name</oldsource>
        <translation>חפש ספר לפי &amp;שם</translation>
    </message>
    <message>
        <source>font: 8pt &quot;Sans Serif&quot;;</source>
        <translatorcomment>?</translatorcomment>
        <translation type="obsolete">font: 8pt &quot;Sans Serif&quot;;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1076"/>
        <source>( The search ignores Punctuation,
        Nikud and Teamim. )
</source>
        <translation>(החיפוש מתעלם מפיסוק, ניקוד וטעמים)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1490"/>
        <source>Edit bookmark&apos;s title</source>
        <translation>ערוך כותרת סימנייה</translation>
    </message>
    <message utf8="true">
        <source>אָ֣ז יָשִֽׁיר־מֹשֶׁה֩</source>
        <translation type="obsolete">אָ֣ז יָשִֽׁיר־מֹשֶׁה֩</translation>
    </message>
</context>
<context>
    <name>OraytaBookView</name>
    <message>
        <location filename="bookview_orayta.cpp" line="19"/>
        <source>Add bookmark here...</source>
        <translation>הוסף סימנייה כאן...</translation>
    </message>
    <message>
        <location filename="bookview_orayta.cpp" line="20"/>
        <source>Add/edit comment...</source>
        <translation>הוסף/ערוך הערה...</translation>
    </message>
    <message>
        <location filename="bookview_orayta.cpp" line="21"/>
        <source>Delete comment</source>
        <translation>מחק הערה</translation>
    </message>
    <message>
        <location filename="bookview_orayta.cpp" line="22"/>
        <source>Copy text only</source>
        <translation>העתק טקסט בלבד</translation>
    </message>
</context>
<context>
    <name>PdfBookView</name>
    <message>
        <location filename="bookview_pdf.cpp" line="75"/>
        <source>Page </source>
        <translation>דף </translation>
    </message>
</context>
<context>
    <name>PdfWidget</name>
    <message>
        <location filename="pdfwidget.cpp" line="80"/>
        <source>Copy text</source>
        <translation>העתק טקסט</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="functions.cpp" line="496"/>
        <source>LTR</source>
        <translation>RTL</translation>
    </message>
</context>
<context>
    <name>QtIOCompressor</name>
    <message>
        <location filename="qtiocompressor.cpp" line="129"/>
        <location filename="qtiocompressor.cpp" line="595"/>
        <source>Internal zlib error when compressing: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtiocompressor.cpp" line="161"/>
        <source>Error writing to underlying device: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtiocompressor.cpp" line="362"/>
        <source>Error opening underlying device: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtiocompressor.cpp" line="532"/>
        <source>Error reading data from underlying device: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtiocompressor.cpp" line="552"/>
        <source>Internal zlib error when decompressing: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QtIOCompressor::open</name>
    <message>
        <location filename="qtiocompressor.cpp" line="396"/>
        <source>The gzip format not supported in this version of zlib.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtiocompressor.cpp" line="412"/>
        <source>Internal zlib error: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchForm</name>
    <message>
        <source>Advanced search</source>
        <translation type="obsolete">חיפוש מתקדם</translation>
    </message>
    <message>
        <source>String search</source>
        <translation type="obsolete">חיפוש מחרוזות</translation>
    </message>
    <message>
        <source>Find all words</source>
        <translation type="obsolete">חפש את כל המילים</translation>
    </message>
    <message>
        <source>Find any of the words</source>
        <translation type="obsolete">חפש חלק מהמילים</translation>
    </message>
    <message>
        <source>Find full words only</source>
        <translation type="obsolete">חפש מילים שלמות</translation>
    </message>
    <message>
        <source>Find regular expression</source>
        <translation type="obsolete">חפש ביטוי רגולרי</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">ביטול</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="obsolete">חפש</translation>
    </message>
    <message>
        <source>RegExp: </source>
        <translation type="obsolete">ביטוי רגולרי:</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="settings.ui" line="14"/>
        <source>Orayta settings</source>
        <translation>הגדרות אורייתא</translation>
    </message>
    <message>
        <location filename="settings.ui" line="24"/>
        <source>Font</source>
        <translation>גופן</translation>
    </message>
    <message>
        <location filename="settings.ui" line="36"/>
        <source>Default font:</source>
        <translation>פונט ברירת מחדל:</translation>
    </message>
    <message>
        <location filename="settings.ui" line="46"/>
        <source>font: 8pt &quot;Sans Serif&quot;;</source>
        <translation>font: 8pt &quot;Sans Serif&quot;;</translation>
    </message>
    <message>
        <location filename="settings.ui" line="76"/>
        <source>Base font size:</source>
        <translation>גודל גופן בסיס:</translation>
    </message>
    <message>
        <location filename="settings.ui" line="99"/>
        <source>Font preview:</source>
        <translation>תצוגה מקדימה:</translation>
    </message>
    <message>
        <location filename="settings.ui" line="114"/>
        <source>(Make sure the font can handle Nikud and Teamim)</source>
        <translation>(מומלץ לוודא שהפונט תומך בניקוד וטעמים)</translation>
    </message>
    <message utf8="true">
        <location filename="settings.ui" line="254"/>
        <source>אָ֣ז יָשִֽׁיר־מֹשֶׁה֩</source>
        <translation>אָ֣ז יָשִֽׁיר־מֹשֶׁה֩</translation>
    </message>
    <message>
        <location filename="settings.ui" line="285"/>
        <source>(Font confs will apply as of next book)</source>
        <translation>(ההגדרות יחולו החל מהספרים הבאים)</translation>
    </message>
    <message>
        <location filename="settings.ui" line="296"/>
        <source>Language</source>
        <translation>שפה</translation>
    </message>
    <message>
        <location filename="settings.ui" line="316"/>
        <source>Custom language</source>
        <translation>שפה מותאמת אישית</translation>
    </message>
    <message>
        <location filename="settings.ui" line="325"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Arial&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:7pt;&quot;&gt;Note: Even when using a custom language, some dialogs may still appear in the system&apos;s language.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settings.ui" line="345"/>
        <source>Use system language</source>
        <translation>השתמש בשפת המערכת</translation>
    </message>
    <message>
        <source>Note: Changing the language will restart the application, and close all open books</source>
        <translation type="obsolete">שינוי השפה יגרום לאתחול התוכנה, ולסגירת כל הספרים הפתוחים</translation>
    </message>
    <message>
        <location filename="settings.ui" line="380"/>
        <source>Cancel</source>
        <translation>ביטול</translation>
    </message>
    <message>
        <location filename="settings.ui" line="400"/>
        <source>Save settings</source>
        <translation>שמור הגדרות</translation>
    </message>
</context>
<context>
    <name>bookDisplayer</name>
    <message>
        <source>Orayta</source>
        <translation type="obsolete">אורייתא</translation>
    </message>
    <message>
        <source>Add bookmark here...</source>
        <translation type="obsolete">הוסף סימנייה כאן...</translation>
    </message>
    <message>
        <source>Add/edit comment...</source>
        <translation type="obsolete">הוסף/ערוך הערה...</translation>
    </message>
    <message>
        <source>Edit comment...</source>
        <translation type="obsolete">ערוך הערה...</translation>
    </message>
    <message>
        <source>Delete comment</source>
        <translation type="obsolete">מחק הערה</translation>
    </message>
</context>
<context>
    <name>bookfind</name>
    <message>
        <location filename="bookfind.ui" line="14"/>
        <source>find book by name</source>
        <translation>חפש ספר לפי שם</translation>
    </message>
    <message>
        <location filename="bookfind.ui" line="26"/>
        <source>Search in name&apos;s beginning</source>
        <translation>חפש בתחילת השם</translation>
    </message>
    <message>
        <location filename="bookfind.ui" line="36"/>
        <source>Search in the whole name</source>
        <translation>חפש בכל השם</translation>
    </message>
    <message>
        <location filename="bookfind.ui" line="57"/>
        <source>Cancel</source>
        <translation>ביטול</translation>
    </message>
    <message>
        <location filename="bookfind.ui" line="67"/>
        <source>Open book</source>
        <translation>פתח ספר</translation>
    </message>
</context>
<context>
    <name>errorReport</name>
    <message>
        <location filename="errorreport.ui" line="14"/>
        <source>Report typo</source>
        <translation>דווח על טעות</translation>
    </message>
    <message>
        <location filename="errorreport.ui" line="23"/>
        <source>Typo&apos;s location</source>
        <translation>מיקום הטעות</translation>
    </message>
    <message>
        <location filename="errorreport.ui" line="37"/>
        <source>Describe the typo:</source>
        <translation>תיאור מפורט של הטעות:</translation>
    </message>
    <message>
        <location filename="errorreport.ui" line="52"/>
        <source>Cancel</source>
        <translation>ביטול</translation>
    </message>
    <message>
        <location filename="errorreport.ui" line="72"/>
        <source>Send</source>
        <translation>שלח</translation>
    </message>
</context>
<context>
    <name>importBook</name>
    <message>
        <location filename="importbook.ui" line="14"/>
        <source>Import books</source>
        <translation>יבוא ספרים</translation>
    </message>
    <message>
        <location filename="importbook.ui" line="26"/>
        <source>Books to import:</source>
        <translation>הוספת ספרים:</translation>
    </message>
    <message>
        <location filename="importbook.ui" line="32"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Arial&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Imported books will be copied to the user&apos;s book folder. In order to import the books for all users, see &amp;quot;&lt;/span&gt;&lt;a href=&quot;http://code.google.com/p/orayta/wiki/AddingNewBooks&quot;&gt;&lt;span style=&quot; font-size:9pt; text-decoration: underline; color:#0057ae;&quot;&gt;Installing new books&amp;quot;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt; In Orayt&apos;a wiki.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Arial&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Imoported books will be copied to the user&apos;s book folder. In order to import the books for all users, see &amp;quot;&lt;/span&gt;&lt;a href=&quot;http://code.google.com/p/orayta/wiki/AddingNewBooks&quot;&gt;&lt;span style=&quot; font-size:9pt; text-decoration: underline; color:#0057ae;&quot;&gt;Installing new books&amp;quot;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt; In Orayt&apos;a wiki.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="importbook.ui" line="79"/>
        <source>Delete</source>
        <translation>הסר</translation>
    </message>
    <message>
        <location filename="importbook.ui" line="86"/>
        <source>Add folder</source>
        <translation>הוסף תיקיה</translation>
    </message>
    <message>
        <location filename="importbook.ui" line="93"/>
        <source>Add books</source>
        <translation>הוסף ספרים</translation>
    </message>
    <message>
        <location filename="importbook.ui" line="130"/>
        <source>Cancel</source>
        <translation>ביטול</translation>
    </message>
    <message>
        <location filename="importbook.ui" line="150"/>
        <source>Import</source>
        <translation>יבוא</translation>
    </message>
    <message>
        <location filename="importbook.cpp" line="58"/>
        <source>All supported files (*.html *.htm *.txt *.pdf);;Html files(*.htm *.html);;Text files(*.txt);;Pdf files(*.pdf)</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>myWebView</name>
    <message>
        <source>Copy text only</source>
        <translation type="obsolete">העתק טקסט בלבד</translation>
    </message>
    <message>
        <source>Copy text</source>
        <translation type="obsolete">העתק טקסט</translation>
    </message>
</context>
</TS>
